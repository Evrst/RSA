let liste = ref (List.init 1000000 (fun x -> x + 2)) in
   for i = 2 to int_of_float(sqrt 1000001.0) do
      liste := List.filter (fun x -> x mod i != 0 || x = i) !liste
   done;

List.iter (fun item -> print_int item; print_string " ") !liste ;;
print_newline () ;;
